const mongoose = require("mongoose");

let ApartmentSchema = {
	id: Number,

	city_id: Number,
	region_id: Number,
	house_id: Number,

	old_temperature: [ { date: Date, temp: Number } ]
};

module.exports = mongoose.model("Apartment", ApartmentSchema);