const mongoose = require("mongoose");

let HouseSchema = {
	id: Number,
	apartments: [ { type: mongoose.Schema.Types.ObjectId, ref: "Apartment" } ]
};

module.exports = mongoose.model("House", HouseSchema);