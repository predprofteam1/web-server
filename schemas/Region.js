const mongoose = require("mongoose");

let RegionSchema = {
	id: Number,
	houses: [ { type: mongoose.Schema.Types.ObjectId, ref: "House" } ]
};

module.exports = mongoose.model("Region", RegionSchema);