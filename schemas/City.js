const mongoose = require("mongoose");

let CitySchema = {
    id: Number,
    name: String,
    old_temperature: [ { date: Date, temp: Number } ],
    regions: [ { type: mongoose.Schema.Types.ObjectId, ref: "Region" } ]
};

module.exports = mongoose.model("City", CitySchema);