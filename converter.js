const mongoose = require("mongoose");
const sqlite3 = require("sqlite3").verbose();

const Apartment = require("./schemas/Apartment");
const House = require("./schemas/House");
const Region = require("./schemas/Region");
const City = require("./schemas/City");

mongoose.set("debug", true);
mongoose.connect("mongodb://localhost:27017/db", { useNewUrlParser: true, useUnifiedTopology: true });

let cities = {};
let city_regions = {};
let regions_houses = {};
let houses_apartments = {};

(async () => {
	await City.deleteMany({});
	await Region.deleteMany({});
	await House.deleteMany({});
	await Apartment.deleteMany({});

	const db = new sqlite3.Database("db.db");

	db.serialize(async () => {
		db.each("SELECT * FROM city_temp", (err, row) => {
			if (cities[row.city_id]) {
				cities[row.city_id].old_temperature.push({ date: Math.floor(row.time) * 1000, temp: row.temperature });
			} else {
				let city = new City({
					id: row.city_id,
					name: row.city_name,
					old_temperature: [ { date: Math.floor(row.time) * 1000, temp: row.temperature } ],
					regions: []
				});

				cities[row.city_id] = city;
			}
		});

		db.each("SELECT * FROM room_temp", (err, row) => {
			let city = cities[row.city_id];
			let city_id = city._id;

			if (!city_regions[city_id])
				city_regions[city_id] = {};

			let region = city_regions[city_id][row.area_id];

			if (!region) {
				region = new Region({
					id: row.area_id
				});

				city_regions[city_id][row.area_id] = region;
				city.regions.push(region);
				cities[row.city_id] = city;
			}

			let region_id = region._id;

			if (!regions_houses[region_id])
				regions_houses[region_id] = {};

			let house = regions_houses[region_id][row.house_id];

			if (!house) {
				house = new House({
					id: row.house_id
				});

				regions_houses[region_id][row.house_id] = house;
				region.houses.push(house);
			}

			let house_id = house._id;

			if (!houses_apartments[house_id])
				houses_apartments[house_id] = {};

			let apartment = houses_apartments[house_id][row.room_id];

			if (!apartment) {
				apartment = new Apartment({
					id: row.room_id,

					city_id: city.id,
					region_id: region.id,
					house_id: house.id,

					old_temperature: [ { date: Math.floor(row.time) * 1000, temp: row.temperature } ]
				});

				houses_apartments[house_id][row.room_id] = apartment;
				house.apartments.push(apartment);
			} else {
				apartment.old_temperature.push({ date: Math.floor(row.time) * 1000, temp: row.temperature });
				houses_apartments[house_id][row.room_id] = apartment;
			}
		});

		db.close(async () => {
			for (let city of Object.values(cities)) {
				for (let region of Object.values(city_regions[city._id])) {
					for (let house of Object.values(regions_houses[region._id])) {
						for (let apartment of Object.values(houses_apartments[house._id]))
							await apartment.save();
	
						await house.save();
					}
					
					await region.save();
				}
	
				await city.save();
			}
		});
	});
})();