const config = require("config");
const request = require("request-promise-native");

const token = config.get("token");

const performAPIRequest
= async (path) => {
    let response = await request({
        uri: "https://dt.miet.ru/ppo_it/api/" + path,
        headers: {
            "X-Auth-Token": token
        }
    });

    return JSON.parse(response);
};

module.exports = {
    getCityTemperature: async (id) => {
        return (await performAPIRequest(`/${id}/temperature`)).data;
    },

    getApartmentTemperature: async (city_id, region_id, house_id, id) => {
        return (await performAPIRequest(`/${city_id}/${region_id}/${house_id}/${id}/temperature`)).data;
    }
};