const config = require("config");
const mongoose = require("mongoose");
const express = require("express");

const pug = require("pug");

const Apartment = require("./schemas/Apartment");
const House = require("./schemas/House");
const Region = require("./schemas/Region");
const City = require("./schemas/City");

mongoose.set("debug", config.get("debug"));
mongoose.connect("mongodb://localhost:27017/db", { useNewUrlParser: true, useUnifiedTopology: true });

const API = require("./api");

const render_webpage =
(name, opts = null) => {
    let pug_func = pug.compileFile("views/" + name + ".pug");

    let func = 
    async (req, res) => {
        let to_func_opts = opts;

        if (typeof opts === "function")
            to_func_opts = await opts();

        res.end(pug_func(to_func_opts));
    };

    return func;
};

const app = express();

app.get("/", render_webpage("main", 
async () => {
    const cities = await City.find({});

    for (let i in cities) {
        cities[i] = await cities[i].populate("regions").execPopulate();

        for (let j in cities[i].regions) {
            cities[i].regions[j] = await cities[i].regions[j].populate("houses").execPopulate();

            for (let k in cities[i].regions[j].houses) {
                cities[i].regions[j].houses[k] = await cities[i].regions[j].houses[k].populate("apartments").execPopulate();
            }
        }

        let temps = [];

        for (let d of cities[i].old_temperature)
            temps.push(d.temp);

        cities[i].temps = temps;

        let temp = await API.getCityTemperature(cities[i].id);
        cities[i].temperature = temp;
    }

    return {
        cities
    };
}));

app.get("/id/:id", async (req, res) => {
    let apartment = await Apartment.findById(req.params.id);

    if (!apartment)
        return res.redirect("/");

    apartment.temperature = await API.getApartmentTemperature(apartment.city_id, apartment.region_id, apartment.house_id, apartment.id);

    let temps = [];

    for (let d of apartment.old_temperature)
        temps.push(d.temp);

    return render_webpage("apartment", { apartment, temps })(req, res);
});

app.listen(8080, () => { console.log("listening"); });